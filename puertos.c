
#include <stdio.h>
#include <stdint.h>
#include "puertos.h"

void bitSet (MY_PORTD* puntero, int bit)
{
    /*Prende el bit apuntado en todos los casos.*/
    switch (bit)
    {
        case 0://Estoy en puertoB
        {
            (puntero -> bits.bit0) = ON;
            break;
        }
        case 1:
        {
            (puntero -> bits.bit1) = ON;
            break;
        }
        case 2:
        {
            (puntero -> bits.bit2) = ON;
            break;
        }
        case 3:
        {
            (puntero -> bits.bit3) = ON;
            break;
        }
        case 4:
        {
            (puntero -> bits.bit4) = ON;
            break;
        }
        case 5:
        {
            (puntero -> bits.bit5) = ON;
            break;
        }
        case 6:
        {
            (puntero -> bits.bit6) = ON;
            break;
        }
        case 7:
        {
            (puntero -> bits.bit7) = ON;
            break;
        }
        case 8://Ahora estoy en puerto A
        {
             (puntero -> bits.bit8) = ON;
            break;
        }
        case 9:
        {
            (puntero -> bits.bit9) = ON;
            break;
        }
        case 10:
        {
            (puntero -> bits.bit10) = ON;
            break;
        }
        case 11:
        {
            (puntero -> bits.bit11) = ON;
            break;
        }
        case 12:
        {
            (puntero -> bits.bit12) = ON;
            break;
        }
        case 13:
        {
            (puntero -> bits.bit13) = ON;
            break;
        }
        case 14:
        {
            (puntero -> bits.bit14) = ON;
            break;
        }
        case 15:
        {
            (puntero -> bits.bit15) = ON;
            break;
        }
        default:
        {
            printf("Error, numero invalido.\n");
            break;
        }
    }
            
}

void bitClr (MY_PORTD* puntero, int bit)
{
    /*Apaga el bit apuntado en todos los casos*/
    switch (bit)
    {
        case 0://Estoy en puertoB
        {
            (puntero -> bits.bit0) = OFF;
            break;
        }
        case 1:
        {
            (puntero -> bits.bit1) = OFF;
            break;
        }
        case 2:
        {
            (puntero -> bits.bit2) = OFF;
            break;
        }
        case 3:
        {
            (puntero -> bits.bit3) = OFF;
            break;
        }
        case 4:
        {
            (puntero -> bits.bit4) = OFF;
            break;
        }
        case 5:
        {
            (puntero -> bits.bit5) = OFF;
            break;
        }
        case 6:
        {
            (puntero -> bits.bit6) = OFF;
            break;
        }
        case 7:
        {
            (puntero -> bits.bit7) = OFF;
            break;
        }
        case 8://Ahora estoy en puerto A
        {
             (puntero -> bits.bit8) = OFF;
            break;
        }
        case 9:
        {
            (puntero -> bits.bit9) = OFF;
            break;
        }
        case 10:
        {
            (puntero -> bits.bit10) = OFF;
            break;
        }
        case 11:
        {
            (puntero -> bits.bit11) = OFF;
            break;
        }
        case 12:
        {
            (puntero -> bits.bit12) = OFF;
            break;
        }
        case 13:
        {
            (puntero -> bits.bit13) = OFF;
            break;
        }
        case 14:
        {
            (puntero -> bits.bit14) = OFF;
            break;
        }
        case 15:
        {
            (puntero -> bits.bit15) = OFF;
            break;
        }
        default:
        {
            printf("Error, numero invalido.\n");
            break;
        }
    }
            
}

int bitGet (MY_PORTD* puntero, int bit) 
{
    /*En todos los casos utilizando la variable value, obtenemos el valor del
     bit que le pasamos como parametro a la funcion (devolvera 0 o 1).*/
    int value;
    switch (bit)
    {
        case 0://Estoy en puertoB
        {
            value = puntero -> bits.bit0;
            break;
        }
        case 1:
        {
            value = puntero -> bits.bit1;
            break;
        }
        case 2:
        {
            value = (puntero -> bits.bit2);
            break;
        }
        case 3:
        {
            value = puntero -> bits.bit3;
            break;
        }
        case 4:
        {
            value = puntero -> bits.bit4;
            break;
        }
        case 5:
        {
            value = puntero -> bits.bit5;
            break;
        }
        case 6:
        {
            value = puntero -> bits.bit6;
            break;
        }
        case 7:
        {
            value = puntero -> bits.bit7;
            break;
        }
        case 8://Ahora estoy en puerto A
        {
            value = puntero -> bits.bit8;
            break;
        }
        case 9:
        {
            value = puntero -> bits.bit9;
            break;
        }
        case 10:
        {
            value = puntero -> bits.bit10;
            break;
        }
        case 11:
        {
            value = puntero -> bits.bit11;
            break;
        }
        case 12:
        {
            value = puntero -> bits.bit12;
            break;
        }
        case 13:
        {
            value = puntero -> bits.bit13;
            break;
        }
        case 14:
        {
            value = puntero -> bits.bit14;
            break;
        }
        case 15:
        {
            value = puntero -> bits.bit15;
            break;
        }
        default:
        {
            printf("Error, numero invalido.\n");
            break;
        }
    }
    return value;
}

void bitToggle (MY_PORTD* puntero, int bit)
{
/*En todos los casos nos fijamos si el bit esta apagado, si lo esta, lo prendo, 
  y si no lo esta, lo apago.*/ 
    switch (bit)
    {
        case 0://Estoy en puertoB
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 1:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 2:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 3:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 4:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 5:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 6:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 7:
        {
             if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 8://Ahora estoy en puerto A
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 9:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 10:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 11:
        {
             if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 12:
        {
             if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 13:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 14:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        case 15:
        {
            if(bitGet(puntero, bit) == OFF)
            {
                bitSet(puntero, bit);
            }
            else
            {
                bitClr(puntero, bit);
            }
            break;
        }
        default:
        {
            printf("Error, numero invalido.\n");
            break;
        }
    }
            
}

int maskOn (char portname , MY_PORTD* ppd, uint16_t themask)
{
    int counter;        //Para dejar de analizar la máscara
    int theBit = 0;     //Para ver qué bit de la más cara analizamos
    int sizeFlag = 1;   //Para ver si el port es de 8 bits

    MY_PORTD mask;           //Defino la máscara
    mask.twobyte = themask;
    MY_PORTD* pmask = &mask; //Apunto a la máscara

    if ( (portname == 'a') || (portname == 'A') )
    {
	counter = 8 * SIZE_OF_PORTA;  /*El contador está asociado a la cantidad 
                                        de bits que haya que analizar.*/
    }
    else if ( (portname == 'b') || (portname == 'B') )
    {
        counter = 8 * SIZE_OF_PORTB ;
        theBit = 8;    /*Arranco a analizar desde el 8vo bit del portD 
                        (el 1er bit del portB)*/
    }
    else if ( (portname == 'd') || (portname == 'D') )
    {
        counter = 8 * sizeof (MY_PORTD);
        sizeFlag = 0;  //Apago el sizeflag porque estamos trabajando con 16 bits
    }
    else
    {
        return ERROR;  //El char no es un puerto.
    }

    if (sizeFlag && (themask > 255) )
    {
        return ERROR; /*Si trabajamos con 8 bits, la máscara debe tener apagados
                        sus 8 bits más significativos.*/
    }
    for (int maskBit = 0; counter!=0; --counter , ++theBit , ++maskBit)
    {//Voy analizando bit a bit
        if ( bitGet (pmask,maskBit) ) //Si la máscara tiene al bit encendido,
        {
            bitSet (ppd, theBit); //...entonces prendemos ese bit en el port.
        }
    }

    return TODO_OK; //Le informamos a quien haya invocado que aparentemente anduvo todo bien
}


int maskOff (char portname , MY_PORTD* ppd, uint16_t themask)
{
    int counter;        //Para dejar de analizar la máscara
    int theBit = 0;     //Para ver qué bit de la más cara analizamos
    int sizeFlag = 1;   //Para ver si el port es de 8 bits

    MY_PORTD mask;               //Defino la máscara
    mask.twobyte = themask;
    MY_PORTD* pmask = &mask;    //Apunto a la máscara

    if ( (portname == 'a') || (portname == 'A') )
    {
	counter = 8 * SIZE_OF_PORTA; /*El contador está asociado a la cantidad 
                                       de bits que haya que analizar.*/
    }
    else if ( (portname == 'b') || (portname == 'B') )
    {
        counter = 8 * SIZE_OF_PORTB ;
        theBit = 8; /*Arranco a analizar desde el 8vo bit del portD 
                      (el 1er bit del portB)*/
    }
    else if ( (portname == 'd') || (portname == 'D') )
    {
        counter = 8 * sizeof (MY_PORTD);
        sizeFlag = 0;  //Apago el sizeflag porque estamos trabajando con 16 bits
    }
    else
    {
        return ERROR;  //El char no es un puerto.
    }

    if (sizeFlag && (themask > 255) )
    {
        return ERROR; /*Si trabajamos con 8 bits, la máscara debe tener apagados
                        sus 8 bits más significativos.*/
    }
    for (int maskBit = 0; counter!=0; --counter , ++theBit , ++maskBit) 
    {//Voy analizando bit a bit
        if ( bitGet (pmask,maskBit) ) //Si la máscara tiene al bit encendido,
        {
            bitClr (ppd, theBit); //...entonces apagamos ese bit en el port.
        }
    }

    return TODO_OK; //Le informamos a quien haya invocado que anduvo todo bien
}

int maskToggle (char portname , MY_PORTD* ppd, uint16_t themask)
{
    int counter;        //Para dejar de analizar la máscara
    int theBit = 0;     //Para ver qué bit de la más cara analizamos
    int sizeFlag = 1;   //Para ver si el port es de 8 bits

    MY_PORTD mask;               //Defino la máscara
    mask.twobyte = themask;
    MY_PORTD* pmask = &mask;    //Apunto a la máscara

    if ( (portname == 'a') || (portname == 'A') )
    {
	counter = 8 * SIZE_OF_PORTA; /*El contador está asociado a la cantidad 
                                       de bits que haya que analizar.*/
    }
    else if ( (portname == 'b') || (portname == 'B') )
    {
        counter = 8 * SIZE_OF_PORTB ;
        theBit = 8; /*Arranco a analizar desde el 8vo bit del portD (el 1er bit 
                      del portB)*/
    }
    else if ( (portname == 'd') || (portname == 'D') )
    {
        counter = 8 * sizeof (MY_PORTD);
        sizeFlag = 0; //Apago el sizeflag porque estamos trabajando con 16 bits
    }
    else
    {
        return ERROR; //El char no es un puerto.
    }

    if (sizeFlag && (themask > 255) )
    {
        return ERROR; /*Si trabajamos con 8 bits, la máscara debe tener apagados
                        sus 8 bits más significativos.*/
    }
    for (int maskBit = 0; counter!=0; --counter , ++theBit , ++maskBit)    
    {//Voy analizando bit a bit.
        if ( bitGet (pmask,maskBit) ) //Si la máscara tiene al bit encendido,
        {
            bitToggle (ppd, theBit);  //...entonces lo invertimos.
        }
    }

    return TODO_OK; //Le informamos a quien haya invocado que anduvo todo bien.
}

