
#include <stdio.h>
#include <stdint.h>
#include "puertos.h"

void inputs (MY_PORTD*); //Obtiene los datos ingresados por el usuario.
void action (MY_PORTD*, char);//Analiza el dato ingresado y actua en base a este

/*Apaga o prende todos los bits segun el parametro que se le pase (ON o OFF)*/
void bitON_OFF (MY_PORTD*, int); 

void printPort (MY_PORTD*, char); //Imprime en pantalla el estado del puertoA.

int main (void)
{
    MY_PORTD portD; //Creo mi puerto D.
    MY_PORTD* pPortD = &portD; //Puntero al puerto.
    inputs(pPortD); //Vamos a obtener los datos del usuario     
    return 0;
}

void inputs (MY_PORTD* pPuerto)
{
    char caracter, dato;/*Caracter: lo ingresado por el usuario.
                          Dato: Donde guardamos lo ingresado por el usuario.*/
    /*Variable veces es para asegurarnos que se haya ingresado solo 1 caracter*/
    int veces = 0;
                          
    printf ("Instrucciones:\n1) Para encender LED ingrese el numero de este (0 al 7).\n"
            "2) Ingrese 't' para invertir los LEDs (los encendidos se apagaran y los apagados se encenderan).\n"
            "3) Ingrese 'c' para apagar todos y 's' para encenderlos.\n"
            "4) Ingrese 'q' si quiere salir del programa.\n");
    while (((caracter = getchar()) != 'q')) //Con 'q' finaliza el programa.
    {      
        if (caracter >= '0' && caracter <= '7') //Numeros de bits habilitados.
        {
            dato = caracter; //Guardamos el numero
            veces++; //Indicamos que se ingreso un caracter
        }
        else if (caracter == 't' || caracter == 'T')
        {
            dato = caracter;
            veces++; //Indicamos que se ingreso un caracter
        }
        else if (caracter == 'c' || caracter == 'C')
        {
            dato = caracter;
            veces++; //Indicamos que se ingreso un caracter
        }
        else if (caracter == 's' || caracter == 'S')
        {
            dato = caracter;
            veces++; //Indicamos que se ingreso un caracter
        }
        else if(caracter == '\n')
        {
            if (veces == 1) //Si se ingreso unicamente un caracter.
            {
                action(pPuerto, dato); //Analizamos lo ingresado.
                printPort(pPuerto, 'A'); //Imprimimos el estado del puertoA.
                veces = 0;
            }
            else 
            /*Si se ingreso mas de 1 caracter o uno invalido (estos no 
              incrementan el contador "veces".*/
            {
                printf ("Error, datos ingresados incorrectamente.\n");
                veces = 0; //Reseteo el contador.
            }
        }
    }
    printf("Salio correctamente.\n");
}
void action (MY_PORTD* pPuerto, char accion)
{
    int bit, i; /*bit: Numero de bit a analizar.
                 i: Contador.*/
    if (accion >= '0' && accion <= '9')
    {
        bit = ((int) accion - '0') + 8;/*Lo desplazamos 8 bits porque estamos 
                                         actualizando el puerto A.*/
        bitSet(pPuerto, bit);//Prendemos el bit indicado.
    }
    else if (accion == 't' || accion == 'T')
    {
        for (i = 0; i < BITS ; i++)
        {
            bitToggle (pPuerto, i);//Invierto leds (invertimos todo).
        }
    }
    else if (accion == 'c' || accion == 'C')
    {
        bitON_OFF(pPuerto, OFF);//Apagamos todos los leds
    }
    else if (accion == 's' || accion == 'S')
    {
        bitON_OFF(pPuerto, ON);//Prendemos todos los leds
    }
}

void bitON_OFF (MY_PORTD* pPuerto, int state)
{
    int i;
    switch (state)
    {
        case OFF://Se quiere apagar todos los leds
        {
            for (i = 0; i < BITS; i++)
            {
                bitClr (pPuerto, i);//Apagamos todo
            }
            break;
        }
        case ON://Se quiere prender todo.
        {
            for (i = 0; i < BITS; i++)
            {
                bitSet (pPuerto, i);//Prendemos todo
            }
            break;
        }
    }
}

void printPort (MY_PORTD* puntero, char port)
{
    int bit;
    if (port == 'a' || port == 'A')
    {
        printf ("Puerto A: |");
        for (bit = 15; bit >= 8; --bit) 
        {
            int value = bitGet (puntero, bit); //Obtenemos el valor del bit.
            printf ("%d|", value); //Lo imprimimos en pantalla
        }
        printf ("\n");
    }
    else if (port == 'b' || port == 'B')
    {
        printf ("Puerto B: |");
        for (bit = 7; bit >= 0; --bit)
        {
            int value = bitGet (puntero, bit); //Obtenemos el valor del bit.
            printf ("%d|", value); //Lo imprimimos en pantalla
        }
        printf ("\n");
    }
    else if (port == 'd' || port == 'D')
    {
        printf ("Puerto D: |");
        for (bit = 15; bit >= 0; --bit)
        {
            int value = bitGet (puntero, bit);//Obtenemos el valor del bit.
            printf ("%d|", value); //Lo imprimimos en pantalla
        }
        printf ("\n");
    }
}

