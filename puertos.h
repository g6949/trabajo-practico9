/***************************************************************************//**
  @file     puertos.c
  @brief    Libreria que permite emular el funcionamiento de los puertos A, B y
            D del HC11.
  @author   Felipe Mendia, Juan Cruz Kristof y Juan Ignacio Borda Rojas.
 ******************************************************************************/

#ifndef PUERTOS_H
#define PUERTOS_H

/*******************************************************************************
 * INCLUDE HEADER FILES
 ******************************************************************************/



/*******************************************************************************
 * CONSTANT AND MACRO DEFINITIONS USING #DEFINE
 ******************************************************************************/
#define ON 1
#define OFF 0
#define BITS 16
#define SIZE_OF_PORTA	1 //Tamano de los puertos (bytes).
#define SIZE_OF_PORTB	1
#define SIZE_OF_PORTD	2
#define TODO_OK 1
#define ERROR 0
/*Aunque los muchos de estos defines valen 0 o 1, se hicieron para poder 
 modificar (en caso de hacerlo) y entender el codigo mas facilmente */


/*******************************************************************************
 * ENUMERATIONS AND STRUCTURES AND TYPEDEFS
 ******************************************************************************/

typedef struct 
{
    uint16_t bit0 : 1;
    uint16_t bit1 : 1;
    uint16_t bit2 : 1;
    uint16_t bit3 : 1;
    uint16_t bit4 : 1;
    uint16_t bit5 : 1;
    uint16_t bit6 : 1;
    uint16_t bit7 : 1;
    uint16_t bit8 : 1;
    uint16_t bit9 : 1;
    uint16_t bit10 : 1;
    uint16_t bit11 : 1;
    uint16_t bit12 : 1;
    uint16_t bit13 : 1;
    uint16_t bit14 : 1;
    uint16_t bit15 : 1;
    
}BIT; //16 bits individuales.

typedef struct 
{
    uint8_t portA;
    uint8_t portB;
    
}PORTS; //2 bytes, uno para cada puerto.

typedef union 
{
    uint16_t twobyte;//2 bytes totales del puertoD.
    PORTS portD;//Divididos en cada byte de los puertos.
    BIT bits;//Bits individuales.
    
}MY_PORTD;


/*******************************************************************************
 * VARIABLE PROTOTYPES WITH GLOBAL SCOPE
 ******************************************************************************/

// +ej: extern unsigned int anio_actual;+


/*******************************************************************************
 * FUNCTION PROTOTYPES WITH GLOBAL SCOPE
/ ******************************************************************************/

/*Recibe un puntero a un puerto y el numero de bit a encender. No devuelve nada.*/
void bitSet (MY_PORTD*, int);

/*Recibe un puntero a un puerto y el numero de bit a apagar. No devuelve nada.*/
void bitClr (MY_PORTD*, int);

/*Recibe un puntero a un puerto y el numero de bit a analizar. Devuelve 1 si el
 bit esta encendido y 0 s esta apagado.*/
int bitGet (MY_PORTD*, int);

/*Recibe un puntero a un puerto y el numero de bit a analizar. Si este esta 
 prendido, lo apaga, y viceversa.*/
void bitToggle (MY_PORTD*, int);

/*Reciben la letra del puerto a analizar (A, B o D), un puntero al puerto D
 entero y una mascara. maskOn prende los bits (del puerto) que esten prendidos
 en la mascara. maskOff, hace lo mismo pero apagandolos, y maskToggle invierte
 los bits (del puerto) que esten encendidos en la mascara. Finalmente, estas
 devuelven */
int maskOn      (char, MY_PORTD*, uint16_t);
int maskOff     (char, MY_PORTD*, uint16_t);
int maskToggle  (char, MY_PORTD*, uint16_t);

/*******************************************************************************
 ******************************************************************************/

#endif // PUERTOS_H

